const log4js = require('log4js');

let logger = log4js.getLogger();

logger.level = "debug";

logger.info("La aplicación inició correctamente");
logger.warn("Cuidado! Falta la librería x en el sistema");
logger.error("No se encontró la función enviar email");
logger.fatal("No se pudo iniciar la aplicación");

function sumar(x, y) {
    return x + y;
}

module.exports = sumar;