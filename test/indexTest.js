/* eslint-disable no-undef */
const sumar = require('../index');
const assert = require('assert');

describe("Probar la suma de dos números", () => {
    it("Cinco más cinco es diez", () => {
        assert.equal(10, sumar(5, 5));
    });

    it("Cinco más siete no es diez", () => {
        assert.notEqual(10, sumar(5, 7));
    });
});